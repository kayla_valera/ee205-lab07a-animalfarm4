///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 06a - Animal Farm 3
///
/// @file nene.hpp
/// @version 1.0
///
/// Exports data about all nene
///
/// @author @todo yourName <@todo yourMail@hawaii.edu>
/// @brief  Lab 06a - AnimalFarm3 - EE 205 - Spr 2021
/// @date   @todo dd_mmm_yyyy
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "bird.hpp"

namespace animalfarm {

class Nene : public Bird {
public:
	Nene( std::string tagID, enum Color newColor, enum Gender newGender );

	std::string tagID;

	virtual const std::string speak();	
	void printInfo();
};

} // namespace animalfarm

