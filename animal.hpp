///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 06a - Animal Farm 3
///
/// @file animal.hpp
/// @version 1.0
///
/// Exports data about all animals
///
/// @author Kayla Valera <kvalera@hawaii.edu>
/// @brief  Lab 07a - AnimalFarm4 - EE 205 - Spr 2021
/// @date   @todo 27_MAR_2021
///////////////////////////////////////////////////////////////////////////////

#pragma once

using namespace std; 

#include <string>
#include "node.hpp"

namespace animalfarm {

enum Gender { MALE, FEMALE, UNKNOWN };

enum Color { BLACK, WHITE, RED, BLUE, GREEN, PINK, SILVER, YELLOW, BROWN };

class Animal: public Node {
public:  // Constructors
	Animal();
	~Animal();
	
public:  // Member variables
	enum Gender gender;
	std::string species;

public:  // Member functions
	virtual const std::string speak() = 0;
	void printInfo();
	
public:  // Static functions
	static const std::string colorName  (enum Color color) ;
	static const std::string genderName (enum Gender gender) ;

	static const Gender     getRandomGender();
	static const Color      getRandomColor();
	static const bool       getRandomBool();
	static const float      getRandomWeight( const float from, const float to );
	static const std::string getRandomName();
};

} // namespace animalfarm
