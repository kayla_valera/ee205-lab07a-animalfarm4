#pragma once
#include "node.hpp"

namespace animalfarm{
class SingleLinkedList{
protected:
Node* head = nullptr;
public:
	const bool empty() const;
	void push_front(Node* newNode);
	Node* pop_front();
	Node* get_first() const;
	Node* get_next( const Node* currentNode ) const;
										                unsigned int size() const;
												   };
}

