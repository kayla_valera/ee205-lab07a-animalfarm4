///////////////////////////////////////////////////////////////////////////////
///// University of Hawaii, College of Engineering
///// EE 205  - Object Oriented Programming
///// Lab 07a - Animal Farm 4
/////
///// @file aku.cpp
///// @version 1.0
/////
///// @author Kayla Valera <kvalerahawaii.edu>
///// @brief  Lab 07a - AnimalFarm 4 - EE 205 - Spr 2021
///// @date   27_MAR_2021
/////////////////////////////////////////////////////////////////////////////////


#include "animalfactory.hpp"

#include "animal.hpp"
#include "cat.hpp"
#include "dog.hpp"
#include "nunu.hpp"
#include "aku.hpp"
#include "palila.hpp"
#include "nene.hpp"

namespace animalfarm{ 

Animal* AnimalFactory::getRandomAnimal(){
Animal* newAnimal = NULL;
int i = rand() % 6;

	switch(i){
		case 0: newAnimal = new Cat   (Cat::getRandomName(), Animal::getRandomColor(), Animal::getRandomGender()); break;
		case 1: newAnimal = new Dog   (Dog::getRandomName(), Animal::getRandomColor(),  Animal::getRandomGender()); break;
		case 2: newAnimal = new Nunu  ( Animal::getRandomBool(), Animal::getRandomColor(), Animal::getRandomGender() ); break;
		case 3: newAnimal = new Aku   ( Animal::getRandomWeight(2.2,12.4), Animal::getRandomColor(),Animal::getRandomGender()); break;

	}
	return newAnimal;
}
}

