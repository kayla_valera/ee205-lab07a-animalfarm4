///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 07a - Animal Farm 4
///
/// @file main.cpp
/// @version 2.0
///
/// Exports data about all animals
///
/// @author Kayla Valera <kvalera@hawaii.edu>
/// @brief  Lab 07a - AnimalFarm 4 - EE 205 - Spr 2021
/// @date   @todo 27_MAR_2021
///////////////////////////////////////////////////////////////////////////////

#include <iostream>
#include <array>
#include <memory>
#include <list>

#include "animal.hpp"
#include "animalFactory.hpp"
#include "cat.hpp"
#include "dog.hpp"
#include "nunu.hpp"
#include "aku.hpp"
#include "palila.hpp"
#include "nene.hpp"
#include "list.hpp"
#include "node.hpp"

using namespace std;
using namespace animalfarm;

int main() {
	cout << "Welcome to Animal Farm 4" << endl;

///////////////////////////////                ///////////////////////////////
///////////////////////////////  Animal Array  ///////////////////////////////
///////////////////////////////                ///////////////////////////////
srand(time(NULL));
SingleLinkedList animalList;

for( auto i = 0 ; i < 25 ; i++ ) {
animalList.push_front( AnimalFactory::getRandomAnimal() ) ;
				}
	//info print
	cout << endl;
	cout << "List of Animals" << endl;
	cout << "  Is it empty: " << boolalpha << animalList.empty() << endl;
	cout << "  Number of elements: " << animalList.size() << endl;
	


	for( auto animal = animalList.get_first();        // initialize   
	animal != nullptr;                           //  test   
	animal = animalList.get_next( animal )) {   // increment
		   
	cout << ((Animal*)animal)->speak() << endl;
	}

	while( !animalList.empty() ) {
		   Animal* animal = (Animal*) animalList.pop_front();
		      
		      delete animal;
	}


	return 0;
}


	
	

/////////////////////                ///////////////////////////////
///////////////////////////////  Animal Array  ///////////////////////////////
///////////////////////////////                ///////////////////////////////
	

