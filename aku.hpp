///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 06a - Animal Farm 3
///
/// @file aku.hpp
/// @version 1.0
///
/// Exports data about all fish
///
/// @author @todo yourName <@todo yourMail@hawaii.edu>
/// @brief  Lab 06a - AnimalFarm3 - EE 205 - Spr 2021
/// @date   @todo dd_mmm_yyyy
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "fish.hpp"

namespace animalfarm {

class Aku : public Fish {
public:
	Aku( float newWeight, enum Color newColor, enum Gender newGender );

	float weight;
	
	void printInfo();
};

} // namespace animalfarm

